#!/bin/sh

certbot certonly --webroot --webroot-path /var/www/html --cert-name cert \
    -d grookey.yepoleb.at \
    -d minecraft.yepoleb.at \
    --post-hook "systemctl reload apache2"
