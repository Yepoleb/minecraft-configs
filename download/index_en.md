# Austria Minecraft Server Downloads

Downloads of the now shutdown Austria Discord Minecraft server. Thanks to all players who have visited over time!

## Map

[world\_austria\_2020-10-24.zip](world_austria_2020-10-24.zip)

How to install: Extract world to .minecraft/saves

## Overview map

[View online](map/)

## Server config

The full server config is available on [Gitlab](https://gitlab.com/Yepoleb/minecraft-configs).

