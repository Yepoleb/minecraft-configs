# Austria Minecraft Server Downloads

Downloads für den nun abgeschalteten Austria Discord Minecraft Server. Danke an alle Spieler, die uns in der Zeit besucht haben!

## Welt

[world\_austria\_2020-10-24.zip](world_austria_2020-10-24.zip)

Installation: Den Ordner world nach .minecraft/saves extrahieren.

## Übersichtskarte

[Online anschauen](map/)

## Serverkonfiguration

Die vollständige Serverkonfiguration gibt's auf [Gitlab](https://gitlab.com/Yepoleb/minecraft-configs).

