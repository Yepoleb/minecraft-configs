# Minecraft Config

Full server config of a Minecraft server I used to run from 2020-03-13 to 2020-10-18.

## Executables (bin)

* paper.jar - Paper server jar
* mapcrafter - Mapcrafter tiles binary
* mapcrafter_markers - Mapcrafter markers binary
* mcrcon - Rcon client
* mcsh - Alias for mcron with a preset password
* stopminecraft - Cleanly shutdown minecraft using rcon
* mcbackup - Backup minecraft worlds to a directory
* cleandir - Clean up old files

## Server Configs (conf)

* minecraft.service - systemd service for minecraft
* crontab - crontab lines for backup and map
* certbot.sh - Certbot setup script
* apache-minecraft.conf - Apache config for the minecraft domain
* mapcrafter.ini - Mapcrafter config

## Plugins (plugins)

* EssentialsX - Basic commands
* EssentialsXChat - Chat colors
* EssentialsXProtect - Disable mob griefing
* Harbor - Sleep together ;)
* LogBlock - Log and rollback block changes
* LuckPerms - Player permissions
* Vault - Bridge between permissions plugin and EssentialsXChat
* WorldEdit - Required by LogBlock to log mobs

## Minecraft Configs (mc-conf)

Standard paper server configs, see documentation of the respective components.

## Download (download)

* index\_en.html - Index page for the final world download page
* index\_en.md - Text of the index page in markdown format
* index\_de.html, index\_de.md - Same thing in German

## Tools

* PaperMC - Enhanced Minecraft server - https://papermc.io/ 
* mcrcon - Rcon client - https://github.com/Tiiffi/mcrcon
* mapcrafter - Overview map renderer - https://github.com/miclav/mapcrafter/tree/world116

## Additional Software

* MariaDB - LogBlock database
* Apache2 - Webserver for overview map and snapshots
* Debian 10 - Server OS

## Hosting

The server was hostet on a Hetzner CX41 cloud VM (4 vcores, 16GB RAM, 160GB Storage). It cost me 19.08€/month (because that's apparently what everyone is interested in). The downloads are now mounted as a 30GB extra volume on my main webserver. It needs that much because the map has about 40% filesystem overhead caused by all the small PNGs not fitting well into 4kB blocks.

## License

* Custom scripts - CC0
* Config data - Probably does not fall under copyright.

